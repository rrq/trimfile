/*
   This program copies the head part of a file to a new file, and
   removes it from the original file.

See usagedie()
*/
#define  _LARGEFILE64_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFSIZE 1048576

static struct {
    int nextline;
    int quiet_about_small;
    long cutseek;
    char *filename;
    char *outfile;
} arg;

static struct {
    struct {
	int in;
	int save;
    } fd;
    char buffer[ BUFSIZE ];
} var;

static void usagedie() {
    fprintf( stderr, "%s\n", ""
"Usage: [ options ] file\n"
"  -s n = cut out s bytes (use [GgMmKL] or none) (negative from end)\n"
"  -q = be quiet about too small file\n"
"  -t = extend cut out to next newline\n"
"  -o name = save the cut-out with the given name" );
    exit( 1 );
}

static long parse_cutseek(char *text) {
    char *end = 0;
    long int v = strtol( text, &end, 10 );
    if ( strlen( end ) <= 1 ) {
	fprintf( stderr, "End = %s\n", end );
	switch ( *end ) {
	case '\0': return v;
	case 'G': return 1073741824 * v;
	case 'g': return 1000000000 * v;
	case 'M': return 1048576 * v;
	case 'm': return 1000000 * v;
	case 'K': return 1024 * v;
	case 'k': return 1000 * v;
	case 'L': arg.nextline = -1; return v;
	default:
	    break;
	}
    }
    fprintf( stderr, "Error. Bad size '%s'. Use G,g,M,m,K,k,L or none\n", end );
    usagedie();
    return 0; // unreachable
}

static void parse_arguments(int argc,char **argv) {
    int i;
    for ( i = 1; i < argc; i++ ) {
	if ( *argv[i] != '-' ) {
	    if ( arg.filename ) {
		fprintf( stderr, "** Error. Bad argument: %s\n", argv[i] );
		usagedie();
	    }
	    arg.filename = argv[i];
	    continue;
	}
	switch ( argv[i][1] ) {
	case 't':
	    if ( arg.nextline ) {
		fprintf( stderr, "** Error. Multiple '-t' arguments.\n" );
		usagedie();
	    }
	    arg.nextline = 1;
	    break;
	case 'q':
	    if ( arg.quiet_about_small ) {
		fprintf( stderr, "** Error. Multiple '-q' arguments.\n" );
		usagedie();
	    }
	    arg.quiet_about_small = 1;
	    break;
	case 'o':
	    if ( arg.outfile ) {
		fprintf( stderr, "** Error. Multiple '-o' arguments.\n" );
		usagedie();
	    }
	    arg.outfile = argv[i] + 2;
	    if ( *arg.outfile ) {
		break;
	    }
	    if ( ++i >= argc ) {
		fprintf( stderr, "** Error. Missing filename for '-o'.\n" );
		usagedie();
	    }
	    arg.outfile = argv[i];
	    break;
	case 's':
	    if ( arg.cutseek ) {
		fprintf( stderr, "** Error. Multiple '-s' arguments.\n" );
		usagedie();
	    }
	    if ( *(argv[i] + 2) ) {
		arg.cutseek = parse_cutseek( argv[i] + 2 );
	    } else {
		if ( ++i >= argc ) {
		    fprintf( stderr, "** Error. Missing size for '-s'.\n" );
		    usagedie();
		}
		arg.cutseek = parse_cutseek( argv[i] );
	    }
	    if ( arg.cutseek == 0 ) {
		fprintf( stderr, "** Error. size %ld is invalud.\n",
			 arg.cutseek );
		usagedie();
	    }
	    break;
	default:
	    fprintf( stderr, "** Error. Bad argument: %s\n", argv[i] );
	    usagedie();
	}
    }
    if ( arg.filename ) {
#define FLAGS O_DSYNC | O_LARGEFILE | O_NOCTTY
	var.fd.in = open( arg.filename, O_RDWR | FLAGS );
	if ( var.fd.in < 0 ) {
	    perror( arg.filename );
	    exit( errno );
	}
    } else {
	fprintf( stderr, "** Error. Missing filename.\n" );
	usagedie();
    }
    struct stat info;
    if ( fstat( var.fd.in, & info ) ) {
	perror( "file status" );
	exit( errno );
    }
    if ( arg.nextline >= 0 ) {
	if ( arg.cutseek < 0 ) {
	    arg.cutseek += info.st_size;
	    if ( arg.cutseek <= 0 ) {
		if ( arg.quiet_about_small ) {
		    exit( 0 );
		}
		fprintf( stderr, "Error. Too small input file.\n" );
		usagedie();
	    }
	} else {
	    if ( arg.cutseek >= info.st_size ) {
		if ( arg.quiet_about_small ) {
		    exit( 0 );
		}
		fprintf( stderr, "Error. Too small input file.\n" );
		usagedie();
	    }
	}
    }
    if ( arg.outfile ) {
	var.fd.save = creat( arg.outfile, info.st_mode );
	if ( var.fd.save < 0 ) {
	    perror( arg.outfile );
	    exit( errno );
	}
    }
}

// Read up to max bytes into var.buffer
static int read_block(int fd,int max,char *filename) {
    if ( max > BUFSIZE ) {
	max = BUFSIZE;
    }
    //fprintf( stderr, "Reading %d bytes from %d\n", max, fd );
    int n = read( fd, var.buffer, max );
    if ( n < 0 ) {
	perror( filename );
	exit( errno );
    }
    return n;
}

// Read up to max bytes from buffer, starting at p
static void write_block(int fd,int p,int max,char *filename) {
    while ( p < max ) {
	int w = write( fd, var.buffer + p, max - p );
	if ( w <= 0 ) {
	    perror( filename );
	    exit( errno );
	}
	p += w;
    }
}

static int find_newline(char *p,int n) {
    char *nl = p;
    char *end = p + n;
    for ( ; nl < end; nl++ ) {
	if ( *nl == '\n' ) {
	    return nl - p + 1;
	}
    }
    return -1;
}

// Return the position of the limit:th newline, or count of newlines
// if limit < 0
static size_t skip_newlines(int fd,int limit) {
    size_t last_nl = 0;
    size_t total = 0;
    size_t count  = 0;
    while ( limit != 0 ) {
	int n = read_block( var.fd.in, BUFSIZE, arg.filename );
	if ( n <= 0 ) {
	    break;
	}
	char *p = var.buffer;
	char *nl = 0;
	while ( ( n-- ) ) {
	    total++;
	    if ( *(p++) == '\n' ) {
		nl = p;
		count++;
		if ( --limit <= 0 ) {
		    if ( limit == 0 ) {
			break;
		    }
		    limit = -1;
		}
	    }
	}
	if ( nl ) {
	    last_nl = total - ( p - nl );
	}
    }
    return limit < 0 ? count : last_nl;
}

// Map arg.countseek as a newline measure into byte measure
static void find_cutseek_as_line_count() {
    fprintf( stderr, "arg.cutseek first = %ld\n", arg.cutseek );
    if ( arg.cutseek >= 0 ) {
	arg.cutseek = skip_newlines( var.fd.in, arg.cutseek );
	fprintf( stderr, "arg.cutseek then  = %ld\n", arg.cutseek );
	lseek( var.fd.in, 0, SEEK_SET );
	return;
    }
    size_t n = skip_newlines( var.fd.in, arg.cutseek );
    lseek( var.fd.in, 0, SEEK_SET );
    arg.cutseek += n;
    fprintf( stderr, "arg.cutseek now  = %ld by %ld\n", arg.cutseek, n );
    if ( arg.cutseek > 0 ) {
	arg.cutseek = skip_newlines( var.fd.in, arg.cutseek - 1 );
	lseek( var.fd.in, 0, SEEK_SET );
    } else {
	arg.nextline = 0;
	arg.cutseek = 0;
    }
    fprintf( stderr, "arg.cutseek then  = %ld\n", arg.cutseek );
}

int main(int argc,char **argv) {
    parse_arguments( argc, argv );
    if ( arg.nextline == -1 ) {
	// Start of arg.cutseek lines as new cutseek
	find_cutseek_as_line_count();
    }
    // Read and save the head
    long int done = 0;
    while ( done < arg.cutseek ) {
	//fprintf( stderr, "Read after %ld bytes\n", done );
	int n = read_block( var.fd.in, arg.cutseek - done, arg.filename );
	//fprintf( stderr, "Got %d bytes\n", n );
	if ( n == 0 ) {
	    break; // EOF
	}
	done += n;
	if ( arg.outfile ) {
	    write_block( var.fd.save, 0, n, arg.outfile );
	}
    }
    //fprintf( stderr, "Lift the rest\n" );
    // Copy up the rest
    long int head = 0;
    for ( ;; ) {
	int n = read_block( var.fd.in, BUFSIZE, arg.filename );
	if ( n == 0 ) {
	    break; // EOF
	}
	done += n;
	// Handle line splitting
	int r = 0;
	if ( arg.nextline ) {
	    r = find_newline( var.buffer, n );
	    if ( r < 0 ) {
		if ( arg.outfile ) {
		    write_block( var.fd.save, 0, n, arg.outfile );
		}
		continue;
	    }
	    if ( r > 0 && arg.outfile ) {
		write_block( var.fd.save, 0, r, arg.outfile );
	    }
	    arg.nextline = 0;
	}
	if ( lseek( var.fd.in, head, SEEK_SET ) < 0 ) {
	    perror( "seek head" );
	    exit( errno );
	}
	write_block( var.fd.in, r, n, arg.filename );
	if ( lseek( var.fd.in, done, SEEK_SET ) < 0 ) {
	    perror( "seek tail" );
	    exit( errno );
	}
	head += n - r;
    }
    //fprintf( stderr, "Truncate %s to %ld bytes.\n", arg.filename, head );
    ftruncate( var.fd.in, head );
}

