PROGRAM = trimfile

BINS = $(PROGRAM)
DOCS = $(PROGRAM:%=%.8)

default: $(BINS) $(DOCS)

$(DOCS): %: %.adoc
	a2x -d manpage -f manpage $^

$(PROGRAM): CFLAGS = -Wall
$(PROGRAM): %: %.c

clean:
	rm $(BINS) $(DOCS)
